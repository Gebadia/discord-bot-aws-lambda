from discord.ext import tasks, commands
from discord import Member
import asyncio
import boto3
import time

class ActiveLambda(commands.Cog):
    def __init__(self, id, bot, start_time, lambda_length, timeout_length, app_name, table_name):
        self.index = 0
        self.id = id
        self.bot = bot
        self.lambda_length = lambda_length
        self.timeout_length = timeout_length
        self.app_name = app_name
        self.table_name = table_name
        self.start_time = start_time
        self.__dynamodb_table = None

    def _get_dynamodb_table(self):
        if not self.__dynamodb_table:
            self.__dynamodb_table = boto3.resource('dynamodb').Table(self.table_name)
        return self.__dynamodb_table

    def update_active_lambda(self):
        self._get_dynamodb_table().update_item(
            Key={
                'app': self.app_name,
            },
            UpdateExpression="set id=:i",
            ExpressionAttributeValues={
                ':i': self.id
            },
            ReturnValues="UPDATED_NEW"
        )
    
    def get_time_running(self):
        now = time.time()
        difference = int(now - self.start_time)
        return difference

    async def __check_status(self):
        response = self._get_dynamodb_table().get_item(
            Key={
                'app': self.app_name
            }
        )
        if response['Item']['id'] != self.id:
            for guild in self.bot.guilds:
                await guild.system_channel.send("Murder: " + self.id)
            raise SystemExit
        for guild in self.bot.guilds:
            now = time.time()
            await guild.system_channel.send(self.id + " - " + str(int(now - self.start_time)))

    @commands.Cog.listener()
    async def on_ready(self):
        self.update_active_lambda()
        for guild in self.bot.guilds:
            await guild.system_channel.send("I am alive: " + self.id)

        await asyncio.sleep(self.lambda_length - self.get_time_running())
        self.check.start()

        await asyncio.sleep(self.timeout_length - self.get_time_running())
        for guild in self.bot.guilds:
            await guild.system_channel.send("Stopped myself: " + self.id)
        raise SystemExit

    @tasks.loop(seconds=0.5)
    async def check(self):
        await self.__check_status()
    
    @commands.command()
    async def check_status(self, ctx):
        await self.__check_status()

    @commands.command()
    async def to_the_death_me_lord(self, ctx, *, member: Member = None):
        member = member or ctx.author
        print('I was commanded to stop myself. with !to_the_death by {0.name}~'.format(member))
        await ctx.send('As you wish {0.name}~'.format(member))
        raise SystemExit
