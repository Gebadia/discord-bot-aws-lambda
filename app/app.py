import discord
import asyncio, time
import logging
from active_lambda import ActiveLambda 

APP_NAME = 'discord-app'
TABLE_NAME = 'active-lambdas'
TOKEN = "TOKEN GOES HERE"

def lambda_handler(event, context):
    start_time = time.time()
    LAMBDA_LENGTH = 840
    timeout_length = 870

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    #print(context.get_remaining_time_in_millis()/1000)
    print(event)

    bot = discord.ext.commands.Bot("!")
    bot.add_cog(ActiveLambda(context.aws_request_id, bot, start_time, LAMBDA_LENGTH, timeout_length, APP_NAME, TABLE_NAME))

    try:
        logging.basicConfig(level=logging.INFO)
        bot.run(TOKEN)
    except SystemExit:
        bot.loop.stop()
        bot.loop.close()
        pass

if __name__ == "__main__":
    class Object(object):
        pass
    context = Object()
    context.aws_request_id = input("Enter string aws id :")
    event = {'hi': 'world'}
    lambda_handler(event, context)